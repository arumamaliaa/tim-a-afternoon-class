require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const { image } = require("./models");
const express = require("express");
const fileUpload = require("express-fileupload");
const path = require("path");
const crypto = require("crypto");
const fs = require("fs");

const app = express();
app.use(express.json()); // Enable json req.body
app.use(
  express.urlencoded({
    extended: false,
  })
); // Enable req.body urlencoded
app.use(fileUpload());

// class controller portable
class ImageClass {
  // create image
  create = async (req, res) => {
    // check if there is file uploaded
    if (req.files) {
      // check if files uploaded more than one
      if (req.files.image.length > 1) {
        // loop image
        for (let i = 0; i < req.files.image.length; i++) {
          // upload image by send req, res and index of looping
          await this.uploads(req, res, i);
          // create image data
          await image.create({
            title_id: req.body.title,
            image: req.body.image,
          });
        }
      }
      // upload single image
      await this.uploads(req, res);
      // create image data
      await image.create({
        title_id: req.body.title,
        image: req.body.image,
      });
    }
    return res.status(200).json({
      message: "Success",
    });
  };
  // separated upload image 
  uploads = async (req, res, i) => {
    // create file variable
    let file;
    // if file image uploades is more than one 
    if (req.files.image.length > 1) {
      // use index provided
      file = req.files.image[i];
    } else {
      // set the variable without index
      file = req.files.image;
    }

    if (!file.mimetype.startsWith("image")) {
      return res.status(400).json({ message: "File must be an image " });
    }

    if (file.size > 1000000) {
      return res.status(400).json({ message: "Image must be less than 1MB" });
    }

    let fileName = crypto.randomBytes(16).toString("hex");

    file.name = `${fileName}${path.parse(file.name).ext}`;

    req.body.image = file.name;
    try {
      await file.mv(`./public/${file.name}`, (err) => {
        if (err) {
          console.log(err);

          return res.status(500).json({
            message: "Internal Server Error",
            error: err,
          });
        }
      });
    } catch (error) {
      console.log(error.message);
    }
  };
}

const imageclass = new ImageClass();

app.use("/upload", imageclass.create);

const PORT = 5000 || process.env.PORT;
if (process.env.NODE_ENV !== "test") {
  app.listen(PORT, () => console.log(`Server running on ${PORT}!`));
}
